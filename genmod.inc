<?php

/**
 * @file
 * Genmod Functions
 */

/**
 * Genmod Page Handler
 *
 * @arg
 *   Page argument
 */
function genmod_page_handler($path) {
  /* startup configuration for Genmod */
  //genmod_default_configuration();
  genmod_read_configuration();

  switch ($path) {
    case 'search':
    case 'pedigree':
    case 'quick_start':
    default:
      genmod_load_file("includes/functions", 'php'); // load generic Genmod functions
      //genmod_load_file("includes/functions_language", 'php'); // load Genmod language functions
      genmod_load_file("includes/functions_name", 'php'); // load Genmod name functions
      genmod_load_file("includes/functions_privacy", 'php'); // load Genmod privacy functions
      module_load_include('inc', 'genmod', 'includes/functions_privacy'); // load overiden privacy functions
      module_load_include('inc', 'genmod', 'includes/functions_db'); // load printing functions
      module_load_include('inc', 'genmod', 'includes/functions_print'); // load printing functions
      module_load_include('inc', 'genmod', 'includes/functions_rtl'); // load RTL functions
      module_load_include('inc', 'genmod', 'includes/user_class'); // load User class
      //$global_variables = array('gm_lang', 'GEDCOM', 'GEDCOMS', 'ALLOW_CHANGE_GEDCOM', 'TEXT_DIRECTION', 'gm_username');
      $global_variables = array_merge($_REQUEST, $GLOBALS);
      if (!$output = genmod_load_file($path, 'php', $global_variables, FALSE)) {
        $output = __FUNCTION__ . "(): FIXME: path=$path";
        return $output;
      }
      module_load_include('inc', 'genmod', 'includes/genmod.callback'); // load callback functions
      $content = genmod_callback_buffer($content);
  }
  return $output;
}

/**
 * Load Genmod include file
 *
 * @file
 *   Filename to load
 * @ext
 *   Filename extension
 */
function genmod_load_file($file, $ext = 'php', $variables = array(), $return_variable = FALSE, $global_variables = array()) {
  global $Users;
  $gm_path = drupal_get_path('module', 'genmod') . '/' . GM_PATH;
  $gm_file = "$gm_path/$file.$ext";
  if (!empty($file) && file_exists($gm_file)) {
    foreach ($global_variables as $global_variable) {
        global $$global_variable; // load global variables
    }
    extract($variables);
    ob_start(); // start buffering
    require_once "$gm_file";
    $output = ob_get_contents(); // get content of buffer
    ob_end_clean(); // end buffering
    if (empty($return_variable)) {
      return $output; // return content
    } else {
        return $$return_variable;
    }
  } else {
  $err_msg = t("Can't include %file!", array("%file" => $gm_file));
  return FALSE;
  }
}

/**
 * Get Block Content
 */
function genmod_load_block($block_name, $function_name, $includes = array(), $variables = array()) {
  /* load needed files */
  foreach ($includes as $file) {
    genmod_load_file("includes/$file", 'php', $variables);
  }
  genmod_load_file("blocks/$block_name", 'php', $variables);
  module_load_include('inc', 'genmod', 'includes/genmod.callback'); // load callback definition

  ob_start(); // start buffering content
  $function_name(TRUE, "", NULL, NULL); // call Genmod function
  $content = ob_get_contents(); // get output into variable
  ob_end_clean(); // clear buffer
  $content = genmod_callback_buffer($content);
  return $content; // return output of content
}

/**
 * Convert Genmod path to Drupal
 *
 *  Example conversion:
 *   Old style: index.php?view=preview&q=genmod
 *   New style: index/view/preview/q/genmod
 */
function genmod_convert_old_path($gpath) {
  $new_path = str_replace(array('.php?', '?', '&', '='), '/', $gpath);
  $new_path = str_replace('.php', '', $new_path);
  // FIXME: what if clean url is disabled?
  return $new_path;
}

/**
 * Check for menu items to ignore from Genmod
 *
 */
function genmod_ignore_menu_items($gpath) {
  // FIXME: this functionality could be moved to Configuration Page
  switch ($gpath) {
  case '?view=preview&q=genmod':
    return TRUE;
  default:
    return FALSE;
  }
}

function EscapeQuery($text) {
  return db_escape_string($text); // FIXME: http://drupal.org/node/450020
}

/**
 * function to build an URL querystring from GET or POST variables
 * @return string
 */
/*
function GetQueryString($encode=false) {
  $qstring = "";
  if (!empty($_GET)) {
    foreach($_GET as $key => $value) {
      if($key != "view" && $key != 'html' && $key != "NEWLANGUAGE" && $key != "changelanguage") {
        $qstring .= $key."=".$value."&";
      }
    }
  }
  else {
    if (!empty($_POST)) {
      foreach($_POST as $key => $value) {
        if($key != "view" && $key != 'html' && $key != "NEWLANGUAGE" && $key != "changelanguage") {
          $qstring .= $key."=".$value."&";
        }
      }
    }
  }
  // Remove the trailing ampersand to prevent it from being duplicated
  $qstring = substr($qstring, 0, -1);
  if ($encode) return urlencode($qstring);
  else return $qstring;
}
*/

