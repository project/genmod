
INSTALLATION REQUIREMENTS:

1. Download Genmod sources from http://sourceforge.net/projects/genmod/ and place it inside genmod/ sub-directory
So in example: path_to_modules/genmod/genmod/config.php file should be accessible.

2. Download CCK module if you don't have already:
http://drupal.org/project/cck


IMPORTANT!
After download, please edit genmod/config.php and delete or comment following line:
//require_once("includes/session.php");

