<?php

/**
 * Quick Start Block
 */
function genmod_block_get_quick_start() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/authentication');
  return genmod_load_block('quick_start', 'print_quickstart_block');
}

/**
 * Genmod Welcome Block
 */
function genmod_block_get_welcome() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/functions_rtl');
  return genmod_load_block('gedcom_block', 'print_gedcom_block');
}

/**
 * User Welcome Block
 */
function genmod_block_get_user_welcome() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('user_welcome', 'print_welcome_block', array('functions_date', 'functions_name'));
}

/**
 * Favorites Block
 */
function genmod_block_get_favorites() {
  module_load_include('inc', 'genmod', 'includes/functions_language');
  module_load_include('inc', 'genmod', 'includes/authentication');
  return genmod_load_block('gedcom_favorites', 'print_gedcom_favorites');
}

/**
 * News Block
 */
function genmod_block_get_news() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('gedcom_news', 'print_gedcom_news');
}

/**
 * Statistics Block
 */
function genmod_block_get_statistics() {
  module_load_include('inc', 'genmod', 'includes/functions_db');
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/functions_rtl');
  return genmod_load_block('gedcom_stats', 'print_gedcom_stats');
}

/**
 * Random Media Block
 */
function genmod_block_get_random_media() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('random_media', 'print_random_media', array(), array("MULTI_MEDIA" => TRUE));
}

/**
 * On This Day Events Block
 */
function genmod_block_get_todays_events() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('todays_events', 'print_todays_events');
}

/**
 * Recent Changes Block
 */
function genmod_block_get_recent_changes() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('recent_changes', 'print_recent_changes');
}

/**
 * Review Changes Block
 */
function genmod_block_get_review_changes() {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  return genmod_load_block('review_changes', 'review_changes_block');
}

/**
 * Top Pageviews Block
 */
function genmod_block_get_top_pageviews($no) {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/functions_db');
  return genmod_load_block('top10_pageviews', 'top10_pageviews');
}

/**
 * Top Surnames Block
 */
function genmod_block_get_top_surnames($no) {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/functions_db');
  return genmod_load_block('top10_surnames', 'print_block_name_top10');
}

/**
 * Upcoming Events Block
 */
function genmod_block_get_upcoming_events($no) {
  module_load_include('inc', 'genmod', 'includes/functions_print');
  module_load_include('inc', 'genmod', 'includes/functions_db');
  return genmod_load_block('upcoming_events', 'print_upcoming_events');
}
