<?php

/**
 * Implementation of menu callback
 */
function genmod_settings_form(&$form_state) {

  $form['genmod_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Genmod General settings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

/* Some example:
  $node_types = node_get_types('names'); // get content types
  $content_types = array_merge(array('' => t('<None>')), $node_types);

  // add node_form
  $form['genmod_settings']['content_types'] = array(
    '#type' => 'select',
    '#title' => t('Content Type'),
    '#default_value' => variable_get('genmod_content_types', ''),
    '#options' => $content_types,
  );
*/
  $options_yes_no = array('0' => 'No', '1' => 'Yes');
  $form['genmod_settings']['show_married_names'] = array(
    '#type' => 'select',
    '#title' => t('Show Married Names'),
    '#default_value' => variable_get('genmod_show_married_names', 1),
    '#options' => $options_yes_no,
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save Configuration'),
  );

  return $form;
}

/*
 * Implementation of hook_form_submit
 *
 */
function genmod_settings_form_submit($form, &$form_state) {
  variable_set('genmod_show_married_names', $form_state['values']['show_married_names']);

  drupal_set_message('The configuration has been saved.');
}

/**
 * A central administrative page for Genmod
 */
function genmod_admin_page() {
  // TODO: Create nice admin page

  module_load_include('inc', 'system', 'system.admin');
  $output .= system_admin_menu_block_page();

  return $output;
}
 
