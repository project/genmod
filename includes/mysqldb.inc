<?php
/**
 * MySQL layer for Genmod
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package Genmod module
 */

class MysqlDb {
	
	function MysqlDb() {
		$this->sqlerror = false;
	}
	
	function MakeConnection() {
	  return TRUE; // it's already there
	}
	
	function Query($sql, $noreport) {
		return db_query($sql); // Drupal db query
	}
	
	function EscapeQuery($text) {
		return db_escape_string($text); // FIXME
	}
	
	function HandleError($sql) {
		// do nothing
	}
}

class Result {
		
	function NumRows() {
		$rows = db_num_rows($this->result);
		return $rows;
	}
	
	function FetchAssoc() {
		$rows = db_fetch_array($this->result);
		return $rows;
	}
	
	function FreeResult() {
	  // do nothing
	}
	
	function FetchRow() {
		$rows = db_fetch_row($this->result);
		return $rows;
	}
	
	function InsertID() {
		$rows = db_insert_id($this->result);
		return $rows;
	}
	
	function AffectedRows() {
		$rows = db_affected_rows($this->result);
		return $rows;
	}
}

