<?php

/**
 * Import language strings from file into database
 */
function genmod_locale_import_language($file, $ext = 'txt') {
  include_once './includes/locale.inc';

  $path = drupal_get_path('module', 'genmod') . '/' . GM_PATH . '/' . GM_LANG_PATH;
  $filename = "$file.$ext";
  $fd = fopen("$path/$filename", "rb"); // File will get closed by PHP on return
  if (!$fd) {
    drupal_set_message(t('The translation import failed, because the file %file could not be read.', array('%file' => $filename)));
    return FALSE;
  }

  while (!feof($fd)) {
    $line = fgets($fd, 10*1024); // A line should not be this long
    if ($lineno == 0) {
      // The first line might come with a UTF-8 BOM, which should be removed.
      $line = str_replace("\xEF\xBB\xBF", '', $line);
    }
    $lineno++;
    $line = trim(strtr($line, array("\\\n" => "")));

	$data = preg_split("/\";\"/", $line, 2);
	if (!isset($data[1])) {
	  watchdog('genmod', "$filename: $line", array(), WATCHDOG_ERROR);
	} else {
		$data[0] = substr(trim($data[0]), 1);
		$data[1] = substr(trim($data[1]), 0, -1);
		$sql = "INSERT INTO {genmod_language} (lg_string, lg_english, lg_last_update_date, lg_last_update_by) VALUES ('%s', '%s', '".time()."', 'install')";
		if (!$result = db_query($sql, $data[0], $data[1])) {
			$error_msg = t("Could not add language string %line for language English to table", array('%line' => $line));
			watchdog('genmod', $error_msg);
			drupal_set_message('$error_msg', 'error');
		}
	 }
  }
}

