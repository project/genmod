<?php
/**
 * Core, language related  Functions that can be used by any page in GM
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

/**
 * Remove a language from the database
 *
 * The function removes a language from the database by making the column empty
 * 
 * @param       string       $storelang  The name of the language to remove
 */
function RemoveLanguage_not_needed($removelang) {
    // not needed
}

/**
 * Load the English language
 *
 * This function load English language variables
 *
 * @param       boolean       $return    Whether or not to return the values
 * @param       string       $help    Whether or not to load the help language 
 * @return   array          The array with the loaded language
 */
function LoadEnglish($return=false, $help=false, $altlang = false) {
  global $gm_lang, $TBLPREFIX;
  $sql = "SELECT lg_string, lg_english FROM ".$TBLPREFIX."language" . ($help ? $help : '');
  $result = db_query($sql);
  while ($row = db_fetch_array($result)) { 
    $gm_lang[$row["lg_string"]] = $row["lg_english"];
  }
  return $return ? $gm_lang : NULL;
}

/**
 * Load the English facts
 *
 * This function load English facts language variables
 *
 * @param       boolean       $return    Whether or not to return the values
 * @param       string       $help    Whether or not to load the help language 
 * @return   array          The array with the loaded language
 */
function LoadEnglishFacts($return=false, $help=false, $altlang = false) {
  global $gm_lang, $TBLPREFIX;
  
  $sql = "SELECT lg_string, lg_english FROM ".$TBLPREFIX."facts" . ($help ? $help : '');
  $result = db_query($sql);
  while ($row = db_fetch_array($result)) { 
    $gm_lang[$row["lg_string"]] = $row["lg_english"];
  }
  return $return ? $gm_lang : NULL;
  if ($return) return $gm_lang;
}


/**
 * Load a language into an array
 *
 * Load a language into an array. This can be the general text or the help text 
 * It selects the language from the database and puts it into an array of the
 * format:
 * ["string"] = ["translation"]
 * 
 * @param       string       $language    The language to load
 * @param       boolean       $return    Whether or not to return the values
 * @param       string       $help    Whether or not to load the help language 
 * @return   array          The array with the loaded language
 */
function LoadLanguage_no_needed($language, $return=false, $help=false) {
    // no needed
}

function LoadFacts_no_needed($language, $return=false) {
    // no needed
}

/**
 * Get a language string from the database
 *
 * The function takes the original string and the language in which the string
 * should be translated. From the language table the string in the desired
 * language is retrieved and returned.
 *
 * @author  Genmod Development Team
 * @param    string  $string    The string to retrieve
 * @param    string  $language2  The language the string should be taken from
 * @param    boolean  $help    Should the text be retrieved from the help table
 * @return   string  The string in the requested language
 */
function GetString_no_needed($string, $language2, $file_type="") {
  return __FUNCTION__.'(): FIXME';
  global $TBLPREFIX;

  if ($file_type == "facts") {
    $sql = "SELECT lg_".$language2.", lg_english FROM ".$TBLPREFIX."facts";
    $sql .= " WHERE lg_string = '".$string."'";
  }
  else {
    $sql = "SELECT lg_".$language2.", lg_english FROM ".$TBLPREFIX."language";
    if (substr($string, -5) == "_help") $sql .= "_help";
    $sql .= " WHERE lg_string = '".$string."'";
  }

  $res = NewQuery($sql);
  $row = $res->FetchAssoc($res->result);
  if (empty($row["lg_".$language2])) return $row["lg_english"];
  else return $row["lg_".$language2];

}
/**
 * Write a translated string
 *
 * @param    string  $string  The translated string
 * @param    string  $value  The string to update
 * @param    string  $language2  The language in which the string is translated
 * @return   boolean  true if the update succeeded|false id the update failed
 */

function WriteString_no_needed($string, $value, $language2, $file_type="") {
    // no needed
}

function LoadLangVars_no_needed($langname="") {
    // no needed
}

function StoreLangVars_no_needed($vars) {
    // no needed
}  

// Activate the given language
function ActivateLanguage_no_needed($lang) {
    // no needed
}

// Activate the given language
function DeactivateLanguage_no_needed($lang) {
    // no needed
}

/** Determines whether a language is in use or not
 ** if $lang is empty, return the full array
 ** else return true or false
 */
function LanguageInUse_no_needed($lang="") {
    // no needed
}

/* Get the language file and translation info for the admin messages
 */
function GetLangfileInfo_no_needed($impexp) {
    // no needed
}
