<?php
/**
 * RTL Functions
 *
 * The functions in this file are common to all GM pages and include date conversion 
 * routines and sorting functions.
 *
 * Genmod: Genealogy Viewer
 * Copyright (C) 2005 - 2008 Genmod Development Team
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * @package Genmod module
 */

/**
 * Verify if text is LTR
 *
 * This will verify if text has LTR characters that are not special characters
 *
 * @author 	Genmod Development Team
 * @param		string 	$text	Text to verify
 * @return	boolean	True if LTR characters are present, false if not
 */
function hasLTRText($text) { 
  return FALSE; // always false, let Drupal will handle that
}

/**
 * Verify if text is RTL
 *
 * This will verify if text has RTL characters
 *
 * @author 	Genmod Development Team
 * @param		string 	$text	Text to verify
 * @return	boolean	True if  RTL characters are present, false if not
 */
function hasRTLText($text) {
  return FALSE; // always false, let Drupal will handle that
} 

