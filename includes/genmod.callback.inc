<?php

/**
 * @file
 * Callback functions
 * 
 */

/**
 * Parse Genmod source
 **/
function genmod_callback_buffer($buffer) {
  $buffer = preg_replace('/(\w+).php/', 'genmod/${1}', $buffer);
  return $buffer;
}

